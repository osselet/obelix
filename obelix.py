#!/usr/bin/env python3

"""obelix

This script try to guess the password of each user presents in the /etc/shadow
file of the host, all available CPU will be used.

Using a naive and exhaustive strategy, crypt and compare all possible passwords
with the encrypted ones present in the host /etc/shadow file. Switch to the
next user and display the result when the password is found, or when all
possibilities have been exhausted.

A single user can be targeted with the --user argument.

The set of characters can be configured with -D, -L, -U, -P, and -W. Use
--help for more details.

The work is shared between all CPU. If the --cpu argument was used, the
work will be shared between that number of CPU instead.

USAGE

  obelix.py [-h] [-u USER] [-w WORKERS] [-D] [-L] [-U] [-P] [-W]
  optional arguments:
    -h, --help            show this help message and exit
    -u USER, --user USER  target a specific user
    -w WORKERS, --workers WORKERS
                          the number of worker processes to use (max = cpu)
    -D, --digits          use digits
    -L, --lowercase       use ascii lowercase
    -U, --uppercase       use ascii uppercase
    -P, --punctuation     use punctuation
    -W, --whitespace      use whitespace
    -m MIN, --min MIN     only try passwords with at least this size (default 1)
    -M MAX, --max MAX     only try passwords with at most this size (default 8)

EXAMPLE

    try to guess the password of each local user, using all possible
    combinations of ascii lowercase letters

        sudo ./obelix.py --lowercase

    try to guess the password of each local user, using all possible
    combinations of ascii lowercase and uppercase letters, plus digits

        sudo ./obelix.py --lowercase --uppercase --digits

    same as before, but shorter

        sudo ./obelix.py -L -U -D

    target only the user alice

        sudo ./obelix -L -U -D -u alice

    by default the workload is shared between all CPU by creating one process
    per CPU. this can be lowered, but cannot exceed os.cpu_cout()

        sudo ./obelix.py -L -U -D --workers 4 -u alice

    do nothing, because no set of characters is provided

        sudo ./obelix.py

OUTPUT

    nothing found

        alice...
        |
        +--------[] ?

        bob...
        |
        +--------[] ?

    the password of the user alice is found

        alice...
        |
        +--------[foobar] ?

        bob...
        |
        +--------[] ?

"""

import argparse
from crypt import crypt
from functools import partial
from itertools import product
from multiprocessing import Pool
import os
import string
import sys
import time


ETC_SHADOW = '/etc/shadow'

AVOIDED_SYMBOLS = ['*', '!', '!!']


def _get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-u', '--user', required=False, help='target a specific user'
    )
    parser.add_argument(
        '-w', '--workers', required=False,
        help='the number of worker processes to use (max = cpu)', type=int,
        default=os.cpu_count()
    )
    parser.add_argument(
        '-D', '--digits', required=False, action='store_true',
        help='use digits'
    )
    parser.add_argument(
        '-L', '--lowercase', required=False, action='store_true',
        help='use ascii lowercase'
    )
    parser.add_argument(
        '-U', '--uppercase', required=False, action='store_true',
        help='use ascii uppercase'
    )
    parser.add_argument(
        '-P', '--punctuation', required=False, action='store_true',
        help='use punctuation'
    )
    parser.add_argument(
        '-W', '--whitespace', required=False, action='store_true',
        help='use whitespace'
    )
    parser.add_argument(
        '-m', '--min', required=False, type=int, default=1,
        help='only try passwords with at least this size (default 1)'
    )
    parser.add_argument(
        '-M', '--max', required=False, type=int, default=8,
        help='only try passwords with at most this size (default 8)'
    )
    return parser.parse_args()


def _available_characters(args):
    characters = ''

    if args.digits:
        characters += string.digits

    if args.lowercase:
        characters += string.ascii_lowercase

    if args.uppercase:
        characters += string.ascii_uppercase

    if args.punctuation:
        characters += string.punctuation

    if args.whitespace:
        characters += string.whitespace

    return characters


def _guess_user_password(etc_shadow_line, args):
    user = etc_shadow_line.split(':')[0]
    passwd = etc_shadow_line.split(':')[1]

    if passwd in AVOIDED_SYMBOLS:
        return

    print('{}...'.format(user))
    id_ = passwd.split('$')[1]
    salt = passwd.split('$')[2]

    guessed_password = ''

    for size in range(args.min, args.max + 1):

        # spawn at most one worker per cpu, no more
        if args.workers > os.cpu_count():
            workers = os.cpu_count()
        else:
            workers = args.workers

        with Pool(processes=workers) as pool:

            for guess in pool.imap_unordered(
                partial(_crypt_n_compare, passwd=passwd, id_=id_, salt=salt),
                product(_available_characters(args), repeat=size),
                1
            ):

                # if the truth has been revealed...
                if guess:
                    guessed_password = guess
                    break

            # ...there is no need to continue with this user
            if guessed_password:
                break

    _display_user_result(user, guessed_password)


def _crypt_n_compare(possible, passwd, id_, salt):
    try_ = crypt(''.join(possible), '$' + id_ + '$' + salt)
    if try_ == passwd:
        return ''.join(possible)
    return None


def _display_user_result(user, password=''):
    if password:
        print('|')
        print('+{}[{}] !'.format('-' * (len(user) + 2), password))
    else:
        print('|')
        print('+{}[] ?'.format('-' * (len(user) + 2)))
    print('')


def main():
    """
    Get things done.
    """
    args = _get_args()
    target = ''

    if args.user:
        target = args.user

    with open(ETC_SHADOW) as etc_shadow:
        for line in etc_shadow:
            user = line.split(':')[0]

            if target and target != user:
                continue

            _guess_user_password(line, args)


if __name__ == '__main__':
    main()
    sys.exit(0)
